import socket
import signal  # Identifie les signaux pour kill le programme
import sys  # Utilisé pour sortir du programme
import time
from clientthread import ClientListener

class Server():
    def __init__(self, port):
        self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listener.bind(("", port))
        self.listener.listen(1)
        print("Listening on port", port)
        self.clients_sockets = []
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

    def run(self):
        """ Lance le serveur en arrière-plan """
        while True:
            print("listening new customers")

            try:
                (client_socket, client_adress) = self.listener.accept()
            except:
                sys.exit("Cannot connect clients")

            self.clients_sockets.append(client_socket)
            print("Start the thread for client:", client_adress)
            client_thread = ClientListener(self, client_socket, client_adress)
            client_thread.start()
            time.sleep(0.1)

    def remove_socket(self, socket):
        """ Retire une connexion cliente de la liste des connexions au serveur """
        self.clients_sockets.remove(socket)

    def echo(self, data):
        """ Affiche les informations relatives aux messages envoyés au serveur """
        print("echoing:", data)
        for sock in self.clients_sockets:
            try:
                sock.sendall(data.encode("UTF-8"))
            except:
                print("Cannot send the message")

    def signal_handler(self, signal, frame):
        """ Ferme la connexion en cas de réception d'un signal de fermeture """
        self.listener.close()
        self.echo("QUIT")


if __name__ == "__main__":
    server = Server(59001)
    server.run()
