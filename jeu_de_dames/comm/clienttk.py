from client import Client
import tkinter as tk
from tkinter.scrolledtext import ScrolledText

class ClientTk(Client):
    def __init__(self, username = None, server = None, port = None):
        # Construction de la fenêtre principale "root"
        self.root = tk.Tk()
        self.root.resizable(width = False, height = False)

        # Variable pour l'aspect de l'interface
        self.gris = "#2E3436"  # Gris plutôt sombre
        self.vert = "#73D216"  # Vert clair
        self.police = ("Consolas", "10")

        self.root.config(bg = self.gris)

        if username is None or server is None or port is None:
            self.serveur = tk.StringVar()
            self.username = tk.StringVar()
            self.connexionPort = tk.IntVar()
            self.connexionPort.set(1)
        else:
            super(ClientTk, self).__init__(username, server, port)

    def credits(self):
        """ Retourne les identifiants de connexion du client donnés dans le
        formulaire de connexion. """
        return self.username.get(), self.serveur.get(), self.connexionPort.get()

    def formConnexion(self):
        """ Affiche le formulaire de connexion au serveur de messagerie.
        Les informations demandées sont l'adresse du serveur, le nom de
        l'utilisateur et le numéro de port du serveur. """
        self.root.title("Connexion")

        self.labelServeur = tk.Label(self.root, text = "Serveur :")
        self.labelServeur.pack()

        self.nomServeur = tk.Entry(self.root, textvariable = self.serveur)
        self.nomServeur.pack()
        self.nomServeur.focus_set()

        self.labelUser = tk.Label(self.root, text = "Username :")
        self.labelUser.pack()
        self.nomUser = tk.Entry(self.root, textvariable = self.username)
        self.nomUser.pack()

        self.labelPort = tk.Label(self.root, text = "Port :")
        self.labelPort.pack()
        self.port = tk.Entry(self.root, textvariable = self.connexionPort)
        self.port.pack()

        self.boutonConnect = tk.Button(
            self.root, text = "Connexion", command = self.root.destroy)
        self.boutonConnect.pack(pady = 20)

        # Apparence des étiquettes
        for label in [self.labelServeur, self.labelUser, self.labelPort]:
            label.config(
                bg = self.gris,
                font = self.police,
                fg = self.vert,
                justify = "left",
                pady = 10
            )

        # Apparence des zones d'édition
        for entry in [self.nomServeur, self.nomUser, self.port]:
            entry.config(
                bg = self.gris,
                fg = self.vert,
                font = self.police,
                highlightcolor = self.vert,
                highlightthickness = 2,
                insertbackground = self.vert,
                insertwidth = 8,
                justify = "center",
                relief = "groove",
                selectbackground = self.vert,
                selectforeground = "black"
            )

        # Apparence du bouton
        self.boutonConnect.config(
            activebackground = self.vert,
            activeforeground = "black",
            bg = self.gris,
            fg = self.vert,
            highlightcolor = self.vert
        )

        self.root.geometry("350x275")
        self.root.mainloop()

    def fenetreMessage(self):
        """ Affiche les messages du client reçus par le serveur de messagerie,
        ainsi qu'une zone d'écriture de message pour l'utilisateur du client. """
        self.root.title("Client : " + self.username)

        self.texteUser = tk.StringVar()
        self.textArea = ScrolledText(self.root, width = "50", height = "10",
                                     font = ("Consolas", 12), state = tk.DISABLED)
        self.textArea.grid(row = 0, column = 0, columnspan = 4, sticky = "new")

        self.zoneTexteUser = tk.Entry(self.root, textvariable = self.texteUser)
        self.zoneTexteUser.grid(row = 1, column = 0,
                                columnspan = 3, sticky = "nesw")
        self.zoneTexteUser.bind("<Return>", self.envoiMessage)
        self.zoneTexteUser.focus_set()

        self.boutonEnvoi = tk.Button(
            self.root, text = "Envoyer", command = self.envoiMessage)
        self.boutonEnvoi.grid(row = 1, column = 3, sticky = "nesw")

        # Apparence de la zone d'affichage du texte
        self.textArea.config(
            bg = self.gris,
            font = ("Consolas", "12"),
            fg = self.vert,
            selectbackground = self.vert,
            selectforeground = "black"
        )

        # Apparence de la zone d'entrée de texte de l'utilisateur
        self.zoneTexteUser.config(
            bg = self.gris,
            fg = self.vert,
            font = self.police,
            highlightcolor = self.vert,
            highlightthickness = 2,
            insertbackground = self.vert,
            insertwidth = 8,
            relief = "groove",
            selectbackground = self.vert,
            selectforeground = "black"
        )

        # Apparence du bouton
        self.boutonEnvoi.config(
            activebackground = self.vert,
            activeforeground = "black",
            bg = self.gris,
            fg = self.vert,
            highlightcolor = self.vert
        )

        self.root.geometry("500x250")
        self.listen()
        self.root.mainloop()

    def handle_msg(self, data):
        """ Gère les messages du client.
        Override de la méthode `handle_msg()` de la classe Client. """
        if data == self.username + ": QUIT":
            self.tidy_up()
            self.root.destroy()
        elif data == self.username + ": ":
            self.tidy_up()
            self.root.destroy()
        else:
            self.textArea.configure(state = tk.NORMAL)
            self.textArea.insert(tk.INSERT, data + "\n")
            self.textArea.configure(state = tk.DISABLED)

    def envoiMessage(self, event = None):
        """ Envoie le message du client au serveur et vide le contenu de la
        zone de texte. Cette méthode est aussi appelé lorsque l'utilisateur
        appuie sur la touche "Entrée". """
        self.send(self.texteUser.get())
        self.texteUser.set("")


if __name__ == "__main__":
    client = ClientTk()
    client.formConnexion()
    user, serv, port = client.credits()
    client = ClientTk(user, serv, port)
    client.fenetreMessage()
