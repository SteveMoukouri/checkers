import pygame

WIDTH, HEIGHT = 800, 800  # taille de la fenêtre
ROWS, COLS = 8, 8  # nombre de lignes/colonnes du plateau
CASE_SIZE = WIDTH // COLS  # taille d'une case
FPS = 60  # FPS du jeu
NB_PIECE = 12

# Couleurs
RED = (255, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
BLACK = (0, 0, 0)
GREY = (128, 128, 128)

CROWN = pygame.transform.scale(pygame.image.load("./assets/crown_3.jpg"),(44,25)) #couronne