import pygame
import tools.constants as const
from .piece import Piece

class Plateau:
    def __init__(self):
        """ Constructeur du plateau. """
        self.board = []  # Tableau représentant le plateau
        self.selected_piece = None
        self.rouge_rest = const.NB_PIECE  # Piece rouge restante
        self.blanc_rest = const.NB_PIECE  # Piece blanche restante
        self.create_board()
        self.tour_blanc = True

    def draw_squares(self, window):
        """ Dessine les cases du plateau avec des couleurs alternées. """
        window.fill(const.BLACK)
        for row in range(const.ROWS):
            for col in range(row % 2, const.ROWS, 2):
                pygame.draw.rect(window, const.RED, (row * const.CASE_SIZE, col * const.CASE_SIZE, const.CASE_SIZE, const.CASE_SIZE))

    def create_board(self):
        """ Initialise le tableau du plateau de jeu. """
        for row in range(const.ROWS):
            self.board.append([])
            for col in range(const.COLS):
                if col % 2 == ((row + 1) % 2):
                    if row < 3:
                        self.board[row].append(Piece(row, col, const.WHITE))
                    elif row > 4:
                        self.board[row].append(Piece(row, col, const.RED))
                    else:
                        self.board[row].append(0)
                else:
                    self.board[row].append(0)

    def draw(self, win):
        """ Dessine les pièces sur le plateau (appel de la méthode `draw()` des Pièces). """
        self.draw_squares(win)
        for row in range(const.ROWS):
            for col in range(const.COLS):
                piece = self.board[row][col]
                if piece != 0:
                    piece.draw(win)

    def deplacement (self, row,col,pos):
        """Gere les deplacements sur le plateau (appel la méthode select_case et effectue une serie de test)"""
        select = self.select_case(pos)
        piece = self.board[select[0]][select[1]]
        if (piece == 0):        #cas 1 : selection de case vide
            print("case vide")
        elif (piece.row +1 != row ) and (piece.row - 1 != row ): #cas 2 : on veut se déplacer à une ligne trop eloignee
            print ("piece.row vaut:{} , row vaut {} , bool: + {} or - {}".format(piece.row,row,piece.row == row + 1,piece.row == row - 1))
        elif (piece.col +1 != col ) and (piece.col - 1 != col ): # cas 3 : on veut se déplacer à une colone trop eloignee
             print ("piece.col vaut:{} , col vaut {} , bool: + {} or - {}".format(piece.col,col,piece.col == col + 1,piece.col == col - 1))
        elif (self.board[row][col] != 0): #cas 4 : la case où on veut aller n'est pas vide
            if (self.board[row][col].color == piece.color): # cas 4.1 : la case contient une piece de même couleur
                print("Deplacement impossible")
            else : #cas 4.2 : la case contient une piece de couleur differente
                if(piece.color == const.WHITE and self.tour_blanc):
                    valide = True
                    while(valide):
                        if(col == piece.col +1 and row == piece.row +1 and self.existe(row,col)): #on regarde une diagonale supplémentaire
                            if(row + 1 >= const.ROWS or col + 1 > const.COLS - 1 or self.board[row + 1][col + 1] != 0): # la diagonale est pleine
                                valide = False
                                self.tour_blanc = False
                            elif(self.existe(row,col)): #on peut prendre le pion adverse
                                self.board[piece.row][piece.col], self.board[row + 1][col + 1] = self.board[row + 1][col + 1], self.board[piece.row][piece.col]
                                self.nb_piece(self.board[row][col])
                                self.board[row][col] = 0
                                piece.move(row + 1,col + 1)
                                self.is_King(piece)
                                row, col = row + 2, col + 2

                        elif(col == piece.col - 1 and row == piece.row +1 and self.existe(row,col)): #idem pour la diagonale de gauche
                            if(row + 1 >= const.ROWS  or col - 1 < 0 or self.board[row + 1] [col - 1] !=0):
                                valide = False
                                self.tour_blanc = False
                            elif(self.existe(row,col)):
                                self.board[piece.row][piece.col], self.board[row + 1][col - 1] = self.board[row + 1][col - 1], self.board[piece.row][piece.col]
                                self.nb_piece(self.board[row][col])
                                self.board[row][col] = 0
                                piece.move(row + 1,col  - 1)
                                self.is_King(piece)
                                row, col = row + 2, col -2

                        elif(piece.king and self.existe(row,col)): #cas 5 : la piece est une dame, on ajoute donc 2 deplacements
                            if(row == piece.row - 1):
                        #droite
                                if(col == piece.col +1 and row - 1 >= 0 and col + 1 < const.COLS):
                                    if(self.board[row -1][col +1] == 0):
                                        self.board[piece.row][piece.col], self.board[row - 1][col + 1] = self.board[row - 1][col + 1], self.board[piece.row][piece.col]
                                        self.nb_piece(self.board[row][col])
                                        self.board[row][col] = 0
                                        piece.move(row - 1,col +1)
                                        row, col = row - 2, col + 2
                                    else: 
                                        valide = False
                                        self.tour_blanc = False
                            #gauche
                                else:
                                    if(row - 1 >= 0 and col - 1 >= 0 and self.board[row -1][col -1] == 0):
                                        self.board[piece.row][piece.col], self.board[row - 1][col - 1] = self.board[row - 1][col - 1], self.board[piece.row][piece.col]
                                        self.nb_piece(self.board[row][col])
                                        self.board[row][col] = 0
                                        piece.move(row -1,col -1)
                                        row, col = row - 2, col- 2
                                    else:
                                        valide = False
                                        self.tour_blanc = False
                        else:
                            valide = False
                            self.tour_blanc = False

                elif(self.tour_blanc == False): #La piece est rouge
                    valide_r = True
                    while(valide_r):
                        if(col == piece.col +1 and row == piece.row -1 and self.existe(row,col)): #on regarde une diagonale supplementaire
                            if(row - 1 < 0 or  col + 1 >= const.COLS or self.board[row - 1][col + 1] != 0): # la diagonale est pleine
                                valide_r = False
                                self.tour_blanc = True
                            else: #on peut prendre le pion adverse
                                self.board[piece.row][piece.col], self.board[row - 1][col + 1] = self.board[row - 1][col + 1], self.board[piece.row][piece.col]
                                self.nb_piece(self.board[row][col])
                                self.board[row][col] = 0
                                piece.move(row - 1,col +1)
                                self.is_King(piece)
                                row, col = row - 2, col +2
                                
                        elif(col == piece.col - 1  and row == piece.row -1 and self.existe(row,col)): 
                            if(row - 1 < 0 or  col - 1 < 0 or self.board[row - 1][col - 1] !=0):
                                valide_r = False
                                self.tour_blanc = True
                            else:
                                self.board[piece.row][piece.col], self.board[row - 1][col - 1] = self.board[row - 1][col - 1], self.board[piece.row][piece.col]
                                self.nb_piece(self.board[row][col])
                                self.board[row][col] = 0
                                piece.move(row -1,col -1)
                                self.is_King(piece)
                                row,col = row - 2 , col - 2

                        elif(piece.king and self.existe(row,col)): # cas 5 : la piece est une dame, on ajoute donc 2 deplacements
                            if(row == piece.row +1):
                        #droite :
                                if(col == piece.col + 1 and row + 1 < const.ROWS and col + 1 < const.COLS):
                                    if(self.board[row +1][col +1] == 0):
                                        self.board[piece.row][piece.col], self.board[row + 1][col + 1] = self.board[row + 1][col + 1], self.board[piece.row][piece.col]
                                        self.nb_piece(self.board[row][col])
                                        self.board[row][col] = 0
                                        piece.move(row + 1,col + 1)
                                        row, col = row + 2, col + 2
                                    else:
                                        valide_r = False
                                        self.tour_blanc = True
                        #gauche
                                elif(col == piece.col - 1 and row +1 < const.ROWS and col - 1 >= 0):
                                    if(self.board[row + 1][col -1] == 0):
                                        self.board[piece.row][piece.col], self.board[row + 1][col - 1] = self.board[row + 1][col - 1], self.board[piece.row][piece.col]
                                        self.nb_piece(self.board[row][col])
                                        self.board[row][col] = 0
                                        piece.move(row + 1,col  - 1)
                                        row, col = row +2, col -2
                                    else:
                                        valide_r = False
                                        self.tour_blanc = True
                        else: 
                            valide_r = False
                            self.tour_blanc = True

        else : #cas 6 : la case où on veut aller est vide et dans les règles
            if((piece.color == const.RED and row == piece.row - 1 and not self.tour_blanc)): #cas 5.1 : la piece est rouge
                self.board[piece.row][piece.col], self.board[row][col] = self.board[row][col], self.board[piece.row][piece.col]
                piece.move(row,col)
                self.is_King(piece)
                self.tour_blanc = True

            elif ((piece.color == const.WHITE and row == piece.row + 1)  and self.tour_blanc): #cas 5.2 : la piece est blanche
                self.board[piece.row][piece.col], self.board[row][col] = self.board[row][col], self.board[piece.row][piece.col]
                piece.move(row,col)
                self.is_King(piece)
                self.tour_blanc = False

            elif(piece.king):
                if (self.tour_blanc and piece.color == const.WHITE):
                    self.board[piece.row][piece.col], self.board[row][col] = self.board[row][col], self.board[piece.row][piece.col]
                    piece.move(row,col)
                    self.tour_blanc = False
                elif(piece.color == const.RED and not self.tour_blanc):
                    self.board[piece.row][piece.col], self.board[row][col] = self.board[row][col], self.board[piece.row][piece.col]
                    piece.move(row,col)
                    self.tour_blanc = True

    def select_case(self,pos):
        """Prend en argument la position (x,y) de la souris lors du clic et renvoit la case du plateau (fais appel à select_piece pour la selection) """
        case_col = pos[0] // const.CASE_SIZE
        case_row = pos[1] //const.CASE_SIZE
        self.select_piece(case_row,case_col)
        return (case_row,case_col)

    def select_piece(self,row,col):
        """Selectionne la piece et change la valeur de son booleen correspondant """
        for i in range(const.ROWS):
            for j in range(const.COLS):
                if(self.board[i][j] != 0):
                    self.board[i][j].selected = False
        if(self.board[row][col] != 0):
            self.board[row][col].selected = True
    
    def is_King(self,piece):
        """Actualise le plateau lorsqu'une piece atteint une extremite """                
        if (piece.row == const.ROWS -1 or piece.row == 0):
            piece.king = True

    def nb_piece(self,piece):
        """ Retire une piece """
        if (piece.color == const.RED):
            self.rouge_rest = self.rouge_rest - 1
        else :
            self.blanc_rest = self.blanc_rest - 1
    
    def victoire(self):
        """ Annonce le gagnant """
        if(self.rouge_rest <= 0):
            return "BLANC"
        if(self.blanc_rest <= 0):
            return "ROUGE"
    
    def existe(self, row, col):
        """verifie l'existance de la piece dans le plateau"""
        return row < const.ROWS and row >= 0 and col >= 0 and col < const.COLS and self.board[row][col] != 0