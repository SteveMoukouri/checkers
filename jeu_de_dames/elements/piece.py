import tools.constants as const
import pygame

class Piece:
    PADDING = 15
    OUTLINE = 2

    def __init__(self, row, col, color):
        """ Définition d'une pièce. """
        self.row = row
        self.col = col
        self.color = color
        self.king = False
        self.selected = False
        self.x = 0
        self.y = 0
        self.calc_pos()

    def calc_pos(self):
        """ Détermine la position en x et y de la pièce en fonction de la
        taille des cases. """
        self.x = const.CASE_SIZE * self.col + const.CASE_SIZE // 2
        self.y = const.CASE_SIZE * self.row + const.CASE_SIZE // 2

    def make_king(self):
        """ Transforme une pièce en Dame. """
        self.king = True

    def draw(self, window):
        """ Dessine la pièce dans la bonne case du plateau Pygame. """
        radius = const.CASE_SIZE // 2 - self.PADDING
        pygame.draw.circle(window, const.GREY, (self.x, self.y), radius + self.OUTLINE)
        pygame.draw.circle(window, self.color, (self.x, self.y), radius)
        if self.king: #Lorsque la piece est un roi on dessine la couronne
            window.blit(const.CROWN, (self.x - const.CROWN.get_width()//2, self.y - const.CROWN.get_height()//2))
        self.draw_select(window)

    def move(self,row,col):
        """Deplace la piece à la case correspondante (row,col)"""
        self.row = row
        self.col = col
        self.calc_pos()
    
    def draw_select(self,window):
        """Si la piece est selectionnee, on entoure sa case en bleu"""
        if (self.selected):
            pygame.draw.rect(window,const.BLUE,(self.col*const.CASE_SIZE,self.row*const.CASE_SIZE ,const.CASE_SIZE,const.CASE_SIZE),2)
    
    def __eq__(self, other): 
        """Verifie l'egalite de 2 pieces"""
        if not isinstance(other, Piece):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.row == other.row and self.col == other.col and self.color == other.color 
