import pygame
import tools.constants as const
from elements.plateau import Plateau

class Lancement:
    def __init__(self):
        """ Constructeur vide. """
        print("init")
    
    def end_screen(self,win, text):
        pygame.init()
        pygame.font.init()
        font = pygame.font.SysFont("comicsans", 80)
        txt = font.render(text,1,const.GREY)
        win.blit(txt, (const.WIDTH / 2 - txt.get_width() / 2, 300))
        pygame.display.update()

        pygame.time.set_timer(pygame.USEREVENT+1, 3000)

        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                    run = False
                elif event.type == pygame.KEYDOWN:
                    run = False
                elif event.type == pygame.USEREVENT+1:
                    run = False

    def debut(self):
        """ Initialisation de la fenêtre Pygame. """
        window = pygame.display.set_mode((const.WIDTH, const.HEIGHT))
        pygame.display.set_caption('Dames')
        run = True
        clock = pygame.time.Clock()
        plateau = Plateau()
        selection = False #Indique si on selectionne une piece ou non
        pos, case = None, None #la position du clic et la case correspondante
        gagnant = None

        while run:
            clock.tick(const.FPS)
            for event in pygame.event.get():
                gagnant = plateau.victoire()
                
                if (gagnant != None):
                    self.end_screen(window,"WINNERS : {} !".format(gagnant))
                    run = False
                    
                # Evenement de fermeture de la fenêtre
                if event.type == pygame.QUIT:
                    self.end_screen(window,"WINNERS : {} !".format(gagnant))
                    run = False

                # Evenement produit par un clic
                if event.type == pygame.MOUSEBUTTONDOWN:
                    selection = not selection
                    if selection: #le pion qu'on selectionne
                        pos = pygame.mouse.get_pos()
                        case = plateau.select_case(pos)
                        print("la case est : {}".format(case))
                    elif not selection: #la case où on va
                        pos2 = pygame.mouse.get_pos()
                        case2 =  (pos2[1]//const.CASE_SIZE , pos2[0]//const.CASE_SIZE)
                        print("la case2 vaut : {}".format(case2))
                        plateau.deplacement(case2[0], case2[1],pos)

            plateau.draw(window)
            pygame.display.update()

        pygame.quit()


if __name__ == "__main__":
    print("j'execute le main")
    lancement = Lancement()
    lancement.debut()
